import argparse
import os
import sys
import asyncio

module_path = os.path.abspath(os.path.join("/home/dennis/llp"))
if module_path not in sys.path:
    sys.path.append(module_path)
from lib import (
    get_model_13_04,
    get_simple_chat,
    save_dicts_to_jsonl,
    load_dicts_from_jsonl,
)


def extend_file_name(fp, ext=".jsonl"):
    return fp.replace(ext, "_predicted" + ext)


async def acomplete(inital_test_path):
    raise NotImplementedError()
    chat = get_model_13_04()
    tests = load_dicts_from_jsonl(inital_test_path)
    inputs = [test["input"] for test in tests]
    for test, answer in zip(tests, await chat.ask_batch(inputs)):
        test["prediction"] = answer["answer"]
    new_fp = extend_file_name(inital_test_path)
    save_dicts_to_jsonl(tests, new_fp)
    return


def complete(inital_test_path):
    chat = get_model_13_04()
    tests = load_dicts_from_jsonl(inital_test_path)
    for test in tests:
        test["prediction"] = chat.ask(test["input"])["answer"]
    new_fp = extend_file_name(inital_test_path)
    save_dicts_to_jsonl(tests, new_fp)


def complete_simple(inital_test_path):
    chat = get_simple_chat()
    tests = load_dicts_from_jsonl(inital_test_path)
    for test in tests:
        test["prediction"] = chat.invoke(test["input"]).content
    new_fp = extend_file_name(inital_test_path)
    save_dicts_to_jsonl(tests, new_fp)


def complete_invokable(initial_test_path, chat):
    tests = load_dicts_from_jsonl(initial_test_path)
    for test in tests:
        test["prediction"] = chat.invoke(test["input"]).content
    new_fp = extend_file_name(initial_test_path)
    save_dicts_to_jsonl(tests, new_fp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Predict for tests")
    parser.add_argument(
        "test_initial_path",
        nargs=1,
        type=str,
        help="initial path to test data in jsonl format",
    )
    args, _ = parser.parse_known_args()
    fp = args.test_initial_path[0]
    is_async = False
    if is_async:
        loop = asyncio.get_event_loop()
        tasks = [acomplete(fp)]
        loop.run_until_complete(asyncio.wait(tasks))
        loop.close()
    else:
        complete_simple(fp)
