import argparse
import os
import sys

module_path = os.path.abspath(os.path.join("/home/dennis/llp"))
if module_path not in sys.path:
    sys.path.append(module_path)
from lib import ModelBuilder, load_dicts_from_jsonl, save_dicts_to_jsonl
from langchain.evaluation import load_evaluator
import logging

format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

accuracy_criteria = {
    "accuracy": """
Score 10: The submitted answer is a subset of the expert answer and is fully consistent with it.
Score 9: The submitted answer is a superset of the expert answer and is fully consistent with it.
Score 7: The submitted answer contains all the same details as the expert answer.
Score 3: There is a disagreement between the submitted answer and the expert answer.
Score 1: The answers differ, but these differences don't matter from the perspective of factuality."""
}


def info_handler(result: str):
    return logging.info(result)


def warning_handler(result: str):
    return logging.warning(result)


def error_handler(result: str):
    return logging.error(result)


logging_handlers = {
    1: error_handler,
    3: warning_handler,
    7: info_handler,
    9: info_handler,
    10: info_handler,
    0: info_handler,
}


def extend_file_name(fp, ext=".jsonl"):
    return fp.replace(ext, "_results" + ext)


def base_eval(eval_input):
    model = ModelBuilder.createVseGptModel("openai/gpt-3.5-turbo-0125", 0)
    evaluator = load_evaluator(
        "labeled_score_string",
        criteria=accuracy_criteria,
        llm=model,
    )
    eval_results = evaluator.batch(eval_input)


def eval(test_file_path):
    model = ModelBuilder.createVseGptModel("openai/gpt-3.5-turbo-0125", 0)
    evaluator = load_evaluator(
        "labeled_score_string",
        criteria=accuracy_criteria,
        llm=model,
    )
    eval_input = load_dicts_from_jsonl(test_file_path)
    eval_results = evaluator.batch(eval_input)
    save_dicts_to_jsonl(eval_results, extend_file_name(test_file_path))
    for eval_result in eval_results:
        eval_result = eval_result["results"]
        score = eval_result["score"]
        logging_handlers[score if score in logging_handlers else 0](
            eval_result["reasoning"]
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Eval tests")
    parser.add_argument(
        "test_data_path", nargs=1, type=str, help="path to test data in jsonl format"
    )
    args, _ = parser.parse_known_args()
    eval(args.test_data_path[0])
