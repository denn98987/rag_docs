# Ths project represent my exprirements with llm and RAG
Basic idea get llm, make a RAG. For piplines and interfaces unification used Langchain.
Models downloaded from HigginFace. 
Vectore databases:
- chromadb (slow)
- clickhouse
- faiss (in future)
Problems i've encontered:
- models use a lot of disk space;
- models need a lot of VRAM;
- saiga merged to mistral 7b has bad artifacts connected with bot instructions `<bot>`;
- mistral 7b saiga with lora was fine tuned with bad data, contains some link to sites.\
Solving:
+ Instead of deploy model local use api vsegpt | try google collab 12GB VRAM? | use cloud deploy (expensive);
+ Instead of saiga use original mistral or mistral8x7b;
## Run current demo (demo_28_03)
* Set up envirement vars as in .env.example
* Create ```python3 -m venv venv_compare``` and activate venv ```source venv_compare/bin/activate```
* Install dependencies ```pip install -r ./demo_28_03/requirements.txt``` \
* ~~For embedding: ```python3 ./demo_28_03/scripts/embed.py```~~(deps not freezed yet)
* Run comparing showcase: ```python3 ./demo_28_03/scripts/compare.py```
## Some architectural thoughts
RAG system consist of three parts:
> Module deals with documents on input:
>
> It consist of:
> - document parser (tika)
> - text splitter
> - EMBEDINGS
> - vectore store (clickhouse)
> need to work async, parallel to another module. 

> Module consist of chain for making query: 
> - LLM 
> - EMBEDINGS 

> Module deal with user requests:
> - gradio
## How to deal with apache tika
To up docker container with tika:
```bash
docker pull apache/tika:latest-full #TODO: fix tika version | all dependencies!!!
docker run -it --name tika-server-ocr -d -p 9998:9998 apache/tika:latest-full #TODO: fix tika version | all dependencies!!!
```
TODO: 
To extract text from pdf (texted):
```bash
curl -T  ./txt_lib/ГОСТ_Р_56947.pdf http://localhost:9998/tika --header "Accept: text/plain" > гост_р_56947.txt --header "X-Tika-Skip-Embedded: true"
```
## How to deal with clickhouse
To up docker container with cickhouse
```bash
docker run -d -p 8123:8123 -p9000:9000 --name langchain-clickhouse-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server:24.1.5.6
```
To make embeddings in with langchain interface:
```python
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
settings = ClickhouseSettings(table="table_name")
docsearch = Clickhouse.from_documents(texts_chunks, embeddings, config=settings)
```
Or connect to table with vectorized data:
```python
settings = ClickhouseSettings(table="table_name")
docsearch = Clickhouse(embeddings, config=settings)
```
To make similarity search:
```python
output = docsearch.similarity_search_with_relevance_scores(
    "some request",
    k=#number of pieces,
    where_str=#some additional coonditions,
)
```
As retriever inside qa chain pipeline:
```python
retriever = docsearch.as_retriever(search_kwargs={
    "k": #number of pieces
    })

qa = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=retriever)
```