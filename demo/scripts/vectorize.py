import os
from lib import tokenize
from lib import parse_table_content
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
from langchain.embeddings import HuggingFaceEmbeddings
from langchain_core.documents import Document
from langchain_community.vectorstores import FAISS


def vectorize():
    embeddings = HuggingFaceEmbeddings(
        model_name="ai-forever/sbert_large_nlu_ru",
        model_kwargs={"device": "cuda"},
        encode_kwargs={"normalize_embeddings": True},
    )
    embeddings.show_progress = True

    settings = ClickhouseSettings(table="car_table_09_04")
    clickhouse = Clickhouse(embeddings, config=settings)

    documents = []
    count = 0
    for doc_name, _, files in os.walk("/home/dennis/Downloads/SSP/SSP RUS"):
        for name in files:
            file_path = os.path.join(doc_name, name)
            tc = parse_table_content(file_path)
            if tc is None:
                continue
            try:
                token_data = tokenize(file_path, 256)
            except:
                count += 1
                continue
            for token in token_data:
                metadata = {"chapter_name": token["title"]}
                metadata["file_name"] = name
                documents.append(
                    Document(page_content=token["data"], metadata=metadata)
                )
