# import sys
# if "/home/dennis/llp/DataFlow" not in sys.path:
#     sys.path.append("/home/dennis/llp/DataFlow")

from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime, timedelta
import os
from DataFlow.component.embedder import HuggingFaceEmbedder
from DataFlow.component.vector_store import QdrantVectorStore
from DataFlow.component.config import Config
from DataFlow.process2 import process_document
import dotenv

dotenv.load_dotenv()

config = Config("/home/dennis/llp/DataFlow/config.yaml")
emb_model_name = config.get("embeddings", "qrant")
device = config.get("device")
qrant_url = os.getenv("QRANT_CLOUD_URL")
qrant_api_key = os.getenv("QRANT_API_KEY")
collection_name = config.get("qrant", "collection_name")

# Define the default arguments
default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2023, 1, 1),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=1),
}

embedder = HuggingFaceEmbedder(model_name=emb_model_name, device=device)
vector_store = QdrantVectorStore(
    url=qrant_url, api_key=qrant_api_key, collection_name=collection_name
)

# Define the DAG
dag = DAG(
    "process_and_store_documents",
    default_args=default_args,
    description="A simple DAG to process documents and store in vector DB",
    schedule_interval=timedelta(days=1),
)

# Define the folder to scan for files
FOLDER_PATH = ""


# Function to list files in the folder
def list_files():
    return [
        os.path.join(FOLDER_PATH, f)
        for f in os.listdir(FOLDER_PATH)
        if os.path.isfile(os.path.join(FOLDER_PATH, f))
    ]


# Function to process a single file
def process_file(file_path):
    processed_data = process_document(file_path)
    store_in_vector_db(processed_data)


# Dummy function to represent storing data in vector DB
def store_in_vector_db(data, embedder, vector_store):
    print(f"Storing processed data in vector DB: {data}")

    vector_store.add_document(data=data, embedder=embedder)


# def store_in_vector_db(data):
#     print(f"Storing processed data in vector DB: {data}")
#     url = os.getenv("QRANT_CLOUD_URL")
#     api_key = os.getenv("QRANT_API_KEY")
#     embedding = HuggingFaceEmbeddings(
#         model_name=model_name,
#         model_kwargs={"device": "cuda"},
#         encode_kwargs={"normalize_embeddings": True},
#     )
#     qdrant = Qdrant.from_documents(
#         docs,
#         embeddings,
#         url=url,
#         prefer_grpc=True,
#         api_key=api_key,
#         collection_name="documents",
#     )
# Implement the actual logic to store data in your vector DB

# Task to list files
list_files_task = PythonOperator(
    task_id="list_files",
    python_callable=list_files,
    dag=dag,
)

# Task to process each file
process_files_tasks = []

for file_path in list_files():
    process_file_task = PythonOperator(
        task_id=f"process_file_{os.path.basename(file_path)}",
        python_callable=process_file,
        op_kwargs={"file_path": file_path},
        dag=dag,
    )
    process_files_tasks.append(process_file_task)
    list_files_task >> process_file_task  # Set dependency

# This part dynamically adds tasks to the DAG after listing files
