import pytest
from DataFlow.component.splitter import ChapterSplitter
from DataFlow.models import Metadata, Page, RawDocument, TableContent


@pytest.fixture
def splitter():
    return ChapterSplitter()


def test_split_success(splitter):
    table_content = TableContent(content={"Chapter 1": 1, "Chapter 2": 3})
    pages = [
        Page(page_number=1, text="Content of Chapter 1"),
        Page(page_number=2, text="More content of Chapter 1"),
        Page(page_number=3, text="Content of Chapter 2"),
        Page(page_number=4, text="More content of Chapter 2"),
    ]
    input_data = RawDocument(
        data=pages, metadata=Metadata(file_name="test_file"), is_paged=True
    )
    result = splitter.split(table_content, input_data)

    assert len(result) == 2
    assert result[0].data[0].text == "Content of Chapter 1"
    assert result[1].data[0].text == "Content of Chapter 2"


def test_split_no_content(splitter):
    table_content = TableContent(content={})
    pages = [
        Page(page_number=1, text="Content of the document"),
    ]
    input_data = RawDocument(
        data=pages, metadata=Metadata(file_name="test_file"), is_paged=True
    )
    result = splitter.split(table_content, input_data)

    assert len(result) == 1
    assert result[0].data[0].text == "Content of the document"


def test_split_incomplete_content(splitter):
    table_content = TableContent(content={"Chapter 1": 1, "Chapter 2": 5})
    pages = [
        Page(page_number=1, text="Content of Chapter 1"),
        Page(page_number=2, text="More content of Chapter 1"),
        Page(page_number=3, text="Content of Chapter 2"),
    ]
    input_data = RawDocument(
        data=pages, metadata=Metadata(file_name="test_file"), is_paged=True
    )
    result = splitter.split(table_content, input_data)

    assert len(result) == 2
    assert result[0].data[0].text == "Content of Chapter 1"
    assert result[1].data[0].text == "Content of Chapter 2"


def test_split_metadata_update(splitter):
    table_content = TableContent(content={"Chapter 1": 1, "Chapter 2": 3})
    pages = [
        Page(page_number=1, text="Content of Chapter 1"),
        Page(page_number=2, text="More content of Chapter 1"),
        Page(page_number=3, text="Content of Chapter 2"),
    ]
    input_data = RawDocument(
        data=pages, metadata=Metadata(file_name="test_file"), is_paged=True
    )
    result = splitter.split(table_content, input_data)

    assert result[0].metadata.chapter_title == "Chapter 1"
    assert result[1].metadata.chapter_title == "Chapter 2"


def test_split_empty_document(splitter):
    table_content = TableContent(content={})
    pages = []
    input_data = RawDocument(
        data=pages, metadata=Metadata(file_name="test_file"), is_paged=True
    )
    result = splitter.split(table_content, input_data)

    assert len(result) == 1
    assert result[0].data == []
