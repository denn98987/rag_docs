import logging
from component.load import get_file_paths
from component.converter import doc_to_pdf


def replace_with_pdf(dir_path: str) -> None:
    logging.info(f"replace all word doc/docx with pdfs")
    doc_paths = get_file_paths(dir_path, ["doc", "docx"])
    doc_to_pdf(doc_paths)


if __name__ == "__main__":
    replace_with_pdf()
