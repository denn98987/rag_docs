from typing import Dict, List, Optional
from dataclasses import InitVar, dataclass, MISSING


@dataclass
class Metadata:
    file_name: str
    title: Optional[str] = None
    author: Optional[str] = None
    subject: Optional[str] = None
    keywords: Optional[str] = None
    chapter_title: Optional[str] = None
    first_page_num: Optional[int] = None


@dataclass
class Page:
    page_number: int
    text: str


@dataclass
class RawDocument:
    data: List[Page]
    metadata: Metadata
    is_paged: bool


@dataclass
class TableContentRow:
    page_ind: int
    start_ind: int


@dataclass
class TableContentExt:
    data: Dict[str, TableContentRow]


@dataclass
class TableContent:
    content: Dict[str, int]


@dataclass
class EmbedDocument:
    data: str
    metadata: Dict
