import argparse
import tqdm
from multiprocessing import Pool
import argparse
from typing import List
from DataFlow.component.config import Config
from DataFlow.component.reader import get_reader

# from scripts.convert_to_pdf import replace_with_pdf
from DataFlow.component.text_processor import PDFBookmarkExtractor, DataCleaner
from DataFlow.component.splitter import ChapterSplitter, SentenceSplitter
from DataFlow.models import EmbedDocument, RawDocument

from langchain.schema import Document
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import FAISS

COUNT_P = 12
SUPPORTED_TYPES = ["docx", "doc", "pdf", "txt"]


def read_file(fp: str):
    return get_reader(fp).read_file(fp)


def get_bookmarks(fp: str):
    return (
        PDFBookmarkExtractor().extract_bookmarks(fp),
        PDFBookmarkExtractor().extract_bookmarks(fp, False),
    )


def extract_chapters(file_path: str) -> EmbedDocument:
    reader = get_reader(file_path)
    high_level_tc, low_level_tc = get_bookmarks(file_path)
    doc = reader.read_file(file_path)
    chapters = [doc]
    if high_level_tc is not None:
        chapters = ChapterSplitter().split(high_level_tc, doc)
    elif low_level_tc is not None:
        chapters = ChapterSplitter().split(low_level_tc, doc)
    return chapters


def convert_to_embed_docs(document: RawDocument) -> Document:
    return Document(
        page_content=" ".join([p.text for p in document.data]),
        metadata=document.metadata.__dict__,
    )


def process_document(file_path: str) -> List[Document]:
    chaptered_docs = extract_chapters(file_path)
    cleaned_docs = []
    cleaner = DataCleaner()
    for doc in tqdm.tqdm(chaptered_docs):
        cleaned_docs.append(cleaner.process(doc))
    chunks = []
    with Pool(COUNT_P) as p:
        result = tqdm.tqdm(p.imap_unordered(SentenceSplitter.split, cleaned_docs))
        for doc in result:
            chunks.extend(doc)

    pre_embed_docs = []
    with Pool(COUNT_P) as p:
        result = tqdm.tqdm(p.imap_unordered(convert_to_embed_docs, chunks))
        for doc in result:
            pre_embed_docs.append(doc)

    return pre_embed_docs


# def process_documents(docs_path: str) -> List[Document]:
#     file_paths = get_file_paths(docs_path, SUPPORTED_TYPES)
#     chaptered_docs = []
#     with Pool(COUNT_P) as p:
#         chapters = tqdm.tqdm(p.imap_unordered(extract_chapters, file_paths))
#         for chapter in chapters:
#             chaptered_docs.extend(chapter)
#     cleaned_docs = []
#     cleaner = DataCleaner()
#     for doc in tqdm.tqdm(chaptered_docs):
#         cleaned_docs.append(cleaner.process(doc))
#     # with Pool(COUNT_P) as p:
#     #     cleaned_docs = tqdm.tqdm(p.imap_unordered(cleaner.process, chaptered_docs))
#     # print(chaptered_docs)
#     chunks = []
#     # splitter = SentenceSplitter()
#     with Pool(COUNT_P) as p:
#         result = tqdm.tqdm(p.imap_unordered(SentenceSplitter.split, cleaned_docs))
#         for doc in result:
#             chunks.extend(doc)

#     pre_embed_docs = []
#     with Pool(COUNT_P) as p:
#         result = tqdm.tqdm(p.imap_unordered(convert_to_embed_docs, chunks))
#         for doc in result:
#             pre_embed_docs.append(doc)

#     return pre_embed_docs


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_path", type=str, required=True)
    return parser.parse_args()


def embed(
    documents: List[Document],
    model_name: str = "cointegrated/LaBSE-en-ru",
    path: str = "/home/dennis/llp/data/faiss_cars_01_05",
):
    embedding = HuggingFaceEmbeddings(
        model_name=model_name,
        model_kwargs={"device": "cuda"},
        encode_kwargs={"normalize_embeddings": True},
    )
    db = FAISS.from_documents(documents, embedding)
    db.save_local(path)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_path", type=str, required=True)
    return parser.parse_args()


def main() -> None:
    args = parse_args()
    config = Config(args.config_path)
    doc_dir = config.get("app", "documents_dir")


if __name__ == "__main__":
    main()
