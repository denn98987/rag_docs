import logging
from multiprocessing import Pool
from typing import List
import subprocess
import os
import tqdm


def doc_to_pdf(file_paths: List[str]) -> None:
    with Pool(12) as p:
        for file_path, output in tqdm.tqdm(
            p.imap(convert_to_pdf, file_paths)
        ):  # provide filenames
            if output != 0:
                logging.error(file_path)
            else:
                os.remove(file_path)


def convert_to_pdf(input_path, output_dir=None):
    # Ensure the input file exists
    if not os.path.isfile(input_path):
        raise FileNotFoundError(f"No such file: '{input_path}'")

    # Use the current directory as output directory if not specified
    if output_dir is None:
        output_dir = os.path.dirname(input_path)

    # Ensure the output directory exists
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Construct the command
    command = [
        "libreoffice",
        "--headless",  # Run in headless mode
        "--convert-to",
        "pdf",  # Specify the output format
        "--outdir",
        output_dir,  # Specify the output directory
        input_path,  # Specify the input file
    ]

    # my code
    p = subprocess.Popen(command)
    return input_path, p.wait()
    # Run the command
    # subprocess.run(command, check=True)
