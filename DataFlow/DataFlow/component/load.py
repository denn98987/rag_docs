import glob
import os
from typing import List


def get_file_paths(directory: str, file_types: List[str]) -> List[str]:
    file_paths = []
    for file_type in file_types:
        # Use glob to find files with the specified extension
        file_paths.extend(glob.glob(os.path.join(directory, f"*.{file_type}")))
    return file_paths
