from abc import ABC
from langchain.schema import Document
from langchain_huggingface import HuggingFaceEmbeddings
import typing as tp

from DataFlow.component.config import Config


class BaseEmbedder(ABC):
    embeddings: tp.Any

    @property
    def embeddings(self) -> tp.Any:
        return self._embeddings

    @classmethod
    def from_config(cls, config: Config) -> tp.Any:
        emb_model_name = config.get("embeddings", "default")
        device = config.get("device")
        return cls(emb_model_name, device)


class HuggingFaceEmbedder(BaseEmbedder):
    def __init__(self, model_name, device="cuda", normalize_embeddings=True):
        self._embeddings = HuggingFaceEmbeddings(
            model_name=model_name,
            model_kwargs={"device": device},
            encode_kwargs={"normalize_embeddings": normalize_embeddings},
        )
