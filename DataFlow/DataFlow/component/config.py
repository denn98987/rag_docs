from pathlib import Path

import yaml


class Config:
    def __init__(self, config_path: Path):
        self.config_path = config_path
        self._config = self._load_config()

    def _load_config(self) -> dict:
        with open(self.config_path, "r") as file:
            return yaml.safe_load(file)

    def get(self, *keys, default=None):
        config = self._config
        for key in keys:
            config = config.get(key, default)
            if config is default:
                break
        return config

    def __getitem__(self, item: str):
        return self._config.get(item)

    def __repr__(self) -> str:
        return f"Config({self._config})"
