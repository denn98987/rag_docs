import re
from typing import List, Dict, Optional
from nltk.tokenize import sent_tokenize, word_tokenize

from DataFlow.models import (
    Page,
    RawDocument,
    TableContent,
    Metadata,
    TableContentExt,
    TableContentRow,
)


class ChapterSplitter:
    def split(
        self, table_content: TableContent, input_data: RawDocument
    ) -> List[RawDocument]:
        metadata = input_data.metadata

        pages = input_data.data

        titled_chapters = self.split_chapters(pages, table_content)
        return [
            RawDocument(
                [Page(page_number=0, text=part)],
                self.update_metadata(
                    metadata, chapter_title, table_content.content[chapter_title]
                ),
                False,
            )
            for chapter_title, part in titled_chapters.items()
        ]

    def update_metadata(
        self,
        metadata: Metadata,
        chapter_title: str,
        first_page_num: Optional[int] = None,
    ) -> Metadata:
        # new_metadata = Metadata(
        #     **metadata.__dict__, chapter_title=chapter_title, first_page_num=first_page_num
        # )
        new_metadata = Metadata(
            file_name=metadata.file_name,
            title=metadata.title,
            author=metadata.author,
            subject=metadata.subject,
            keywords=metadata.keywords,
            chapter_title=chapter_title,
            first_page_num=(
                first_page_num
                if first_page_num is not None
                else metadata.first_page_num
            ),
        )
        return new_metadata

    def split_chapters(
        self, pages: List[Page], table_content: TableContent
    ) -> Dict[str, str]:
        table_content_ext = self.find_chapter_start_index(pages, table_content)
        text_parts = {}
        sorted_table_content_items = list(
            sorted(table_content_ext.data.items(), key=lambda item: item[1].page_ind)
        )
        for ind, (chapter_title, indexes) in enumerate(sorted_table_content_items):
            text_pieces = []
            start_chapter_ind = indexes.start_ind
            finish_chapter_ind = (
                sorted_table_content_items[ind + 1][1].start_ind
                if ind + 1 < len(sorted_table_content_items)
                else -1
            )
            start_page_num = min(indexes.page_ind - 1, len(pages) - 1)
            finish_page_num = (
                sorted_table_content_items[ind + 1][1].page_ind - 1
                if ind + 1 < len(sorted_table_content_items)
                else len(pages) - 1
            )

            text_content = pages[start_page_num].text
            text_pieces.append(text_content[start_chapter_ind:])
            start_chapter_ind = 0

            for p_ind in range(start_page_num, min(finish_page_num, len(pages))):
                text_content = pages[p_ind].text
                text_pieces.append(text_content)
            if finish_chapter_ind != 0:
                text_content = pages[finish_page_num].text
                text_pieces.append(text_content[:finish_chapter_ind])
            text_parts[chapter_title] = " ".join(text_pieces).strip()
        return text_parts

    def find_chapter_start_index(
        self, pages: List[Page], table_content: TableContent
    ) -> TableContentExt:
        table_content_ext_data = {}
        for title, page_num in table_content.content.items():
            text = pages[page_num - 1].text
            start_index = text.index(text) if title in text else 0
            table_content_ext_data[title] = TableContentRow(page_num, start_index)
        return TableContentExt(table_content_ext_data)


class SentenceSplitter:
    def split(document: RawDocument, chunk_size: int = 2048) -> List[RawDocument]:
        text = " ".join([p.text for p in document.data])
        sentences = sent_tokenize(text)
        metadata = document.metadata
        docs = []
        sum_sentence = ""
        for sentence in sentences:
            if len(sum_sentence) >= chunk_size:
                docs.append(RawDocument([Page(0, sum_sentence)], metadata, False))
                sum_sentence = ""
            sum_sentence += " " + sentence
        else:
            if len(sum_sentence) > 0:
                docs.append(RawDocument([Page(0, sum_sentence)], metadata, False))
        return docs

    def split_sentences(text: str) -> str:
        return sent_tokenize(text)
