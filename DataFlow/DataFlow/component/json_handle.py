import json
from typing import Iterable


def save_dicts_to_jsonl(array, file_path: str) -> None:
    with open(file_path, "w") as jsonl_file:
        for doc in array:
            jsonl_file.write(json.dumps(doc, ensure_ascii=False) + "\n")


def load_dicts_from_jsonl(file_path) -> Iterable[dict]:
    array = []
    with open(file_path, "r") as jsonl_file:
        for line in jsonl_file:
            data = json.loads(line)
            array.append(data)
    return array
