from typing import List, Optional
from abc import ABC, abstractmethod
import asyncio
import os
from bs4 import BeautifulSoup
import fitz
from tika import parser
import pytesseract
from PIL import Image
from io import StringIO
from DataFlow.models import RawDocument, Metadata, Page


class BaseReader(ABC):
    @abstractmethod
    def read_file(self, file_path: str) -> Optional[RawDocument]:
        pass


class PdfReader(BaseReader):
    def read_file(self, file_path: str) -> Optional[RawDocument]:
        data = []
        metadata = {}
        is_paged = True
        try:
            doc = fitz.open(file_path)
            metadata = {
                "file_name": os.path.basename(file_path),
                "title": doc.metadata.get("title", ""),
                "author": doc.metadata.get("author", ""),
                "subject": doc.metadata.get("subject", ""),
                "keywords": doc.metadata.get("keywords", ""),
            }

            for page_num in range(len(doc)):
                page = doc.load_page(page_num)
                text = page.get_text()
                data.append(Page(page_number=page_num + 1, text=text))

            doc.close()
        except Exception as e:
            print("Error:", e)
            return None

        return RawDocument(data, Metadata(**metadata), is_paged)

    async def read_file_async(self, file_path: str) -> Optional[RawDocument]:
        data = []
        metadata = {}
        is_paged = True
        try:
            doc = fitz.open(file_path)
            metadata = {
                "file_name": os.path.basename(file_path),
                "title": doc.metadata.get("title", ""),
                "author": doc.metadata.get("author", ""),
                "subject": doc.metadata.get("subject", ""),
                "keywords": doc.metadata.get("keywords", ""),
            }

            async def read_page(page_num):
                page = doc.load_page(page_num)
                text = page.get_text()
                return Page(page_number=page_num + 1, text=text)

            tasks = [read_page(page_num) for page_num in range(len(doc))]
            pages = await asyncio.gather(*tasks)
            data = pages

            doc.close()
        except Exception as e:
            print("Error:", e)
            return None

        return RawDocument(data, Metadata(**metadata), is_paged)


class TikaReader(BaseReader):
    def read_file(self, file_path: str) -> Optional[RawDocument]:
        data = []
        metadata = {}
        is_paged = False
        try:
            parsed = parser.from_file(file_path, xmlContent=True)
            content = parsed["content"]

            if content:
                if "<div" in content:
                    is_paged = True
                    pages = self.extract_pages_from_html(content)
                    data = pages
                else:
                    data = [Page(page_number=0, text=content.strip())]

                metadata = {
                    "file_name": os.path.basename(file_path),
                    "title": parsed["metadata"].get("title", ""),
                    "author": parsed["metadata"].get("author", ""),
                    "subject": parsed["metadata"].get("subject", ""),
                    "keywords": parsed["metadata"].get("keywords", ""),
                }

        except Exception as e:
            print("Error:", e)
            return None

        return RawDocument(data, Metadata(**metadata), is_paged)

    def extract_pages_from_html(self, html_content: str) -> List[Page]:
        pages = []
        xhtml_data = BeautifulSoup(html_content, "html.parser")
        for i, content in enumerate(
            xhtml_data.find_all("div", attrs={"class": "page"})
        ):
            _buffer = StringIO()
            _buffer.write(str(content))
            parsed_content = parser.from_buffer(_buffer.getvalue())

            text = parsed_content["content"].strip()
            pages.append(Page(page_number=i + 1, text=text))
        return pages


class OCRReader(BaseReader):
    def read_file(self, file_path: str) -> Optional[RawDocument]:
        data = []
        metadata = {}
        is_paged = False
        try:
            _, file_extension = os.path.splitext(file_path)
            if file_extension.lower() == ".pdf":
                pages = self.extract_text_from_file(file_path)
            elif file_extension.lower() == ".djvu":
                pages = self.extract_text_from_file(file_path)
            else:
                raise ValueError("Unsupported file format")

            if pages:
                is_paged = True
                for i, page_text in enumerate(pages):
                    data.append(Page(page_number=i + 1, text=page_text.strip()))

        except Exception as e:
            print("Error:", e)
            return None

        return RawDocument(
            data,
            Metadata(file_name=os.path.basename(file_path)),
            is_paged,
        )

    def extract_text_from_file(self, file_path: str) -> List[str]:
        pages = []
        doc = fitz.open(file_path)
        for page_num in range(len(doc)):
            page = doc.load_page(page_num)
            image = page.get_pixmap()
            img = Image.frombytes("RGB", (image.width, image.height), image.samples)
            text = pytesseract.image_to_string(img, lang="rus")
            pages.append(text)
        doc.close()
        return pages


def get_reader(file_path: str) -> BaseReader:
    if file_path.endswith(".pdf"):
        return PdfReader()
    return TikaReader()
