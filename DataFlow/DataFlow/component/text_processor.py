from abc import ABC, abstractmethod
import re
import fitz  # PyMuPDF

from typing import List, Dict, Optional
from DataFlow.models import RawDocument, Page, TableContent

TC_SPLITTER = re.compile("[._ ]{3,}")
TC_BR_SPLITTER = re.compile("[._ ]{3,}\n")

DEF = re.compile(r"(\x2D|-)\n?")
NONW = re.compile(r"[^\w\s.,\t\n]")
NUMS = re.compile(r"\d{3}_\d{3}\n")
ESC_SYM = re.compile(r"\\x.")


class DataProcessor(ABC):
    @abstractmethod
    def process(self, data: RawDocument) -> RawDocument:
        pass


class DataCleaner(DataProcessor):
    def process(self, document: RawDocument) -> RawDocument:
        processed_data = []
        for page in document.data:
            processed_text = self.clean_text(page.text)
            processed_data.append(Page(page.page_number, processed_text))
        return RawDocument(processed_data, document.metadata, document.is_paged)

    def clean_text(self, text: str) -> str:
        # Replace \x2D, or -\n with ''
        first_step = re.sub(
            NUMS,
            "",
            str(text)
            .replace("\xad\n", "")
            .replace("\xad", "")
            .replace("\x10\n", "")
            .replace("\x10", "")
            .replace("\x15\n", "")
            .replace("\n–\n", "\n–")
            .replace("-\\n", "")
            .replace("-\n", ""),
        ).replace("\n", " ")

        cleaned_text = re.sub(DEF, "", first_step)
        cleaned_text = re.sub(ESC_SYM, "", cleaned_text)
        # Delete non-word symbols except punctuation marks, meaningful numbers, table symbols, and paragraphs
        cleaned_text = re.sub(NONW, "", cleaned_text).strip()
        return cleaned_text


class TableContentExtractor:
    def extract_table_content(self, data: RawDocument) -> Optional[TableContent]:
        possible_titles = ["Содержание", "Оглавление", "Обзор"]
        for page in data.data.pages:
            page_text = page.text.replace("\n..", "..")
            if any(title in page_text for title in possible_titles):
                lines = page_text.split("\n")
                table_content = self.extract_table_content_forced(lines)
                if len(table_content) > 3:
                    return TableContent(table_content)
                if page.page_number > 6:
                    break
        return None

    def extract_table_content_forced(self, lines: List[str]) -> Dict[str, int]:
        table_content = {}
        for line in lines:
            parts = re.split(TC_SPLITTER, line)
            parts = list(map(lambda part: part.strip(), parts))
            if (
                len(parts) < 2
                or (parts[0].isnumeric() and parts[1].isnumeric())
                or parts[0] == ""
                or not parts[1].isnumeric()
            ):
                continue
            table_content[parts[0]] = int(parts[1])
        return table_content


class PDFBookmarkExtractor:
    def extract_bookmarks(
        self, pdf_path: str, upLevel: Optional[bool]
    ) -> Optional[TableContent]:
        doc = fitz.open(pdf_path)
        bookmarks = self.get_bookmarks(doc, upLevel)
        if bookmarks:
            return TableContent(bookmarks)
        return None

    def get_bookmarks(self, doc: fitz.Document, upLevel: Optional[bool]) -> Dict[str, int]:
        up_bookmarks = {}
        low_bookmarks = {}
        for toc_entry in doc.get_toc():
            level, title, page = toc_entry
            if level > 2:
                up_bookmarks[title] = page
            else:
                low_bookmarks[title] = page
        if upLevel is None:
            return dict(up_bookmarks, **low_bookmarks)
        
        return low_bookmarks if upLevel else up_bookmarks
