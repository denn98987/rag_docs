from __future__ import annotations
from abc import ABC, abstractmethod
from langchain_qdrant import Qdrant
from langchain.schema import Document
import typing as tp
from DataFlow.component.config import Config
from DataFlow.component.embedder import BaseEmbedder
from qdrant_client import QdrantClient


class BaseVectoreStore(ABC):
    def __init__(self, url: str, api_key: str):
        self.url = url
        self.api_key = api_key

    @abstractmethod
    def store(self, data: tp.List[Document], embedder: BaseEmbedder):
        pass


class QdrantVectorStore:
    def __init__(self, url: str, api_key: str, collection_name: str):
        self.url = url
        self.api_key = api_key
        self.collection_name = collection_name

    def store(self, data: tp.List[Document], embedder: BaseEmbedder) -> None:
        Qdrant.from_documents(
            documents=data,
            embedding=embedder.embeddings,
            url=self.url,
            api_key=self.api_key,
            collection_name=self.collection_name,
        )

    def add_document(self, doc: Document, embedder: BaseEmbedder) -> None:
        Qdrant.add_documents(
            docs=[doc],
            embeddings=embedder.embeddings,
            url=self.url,
            prefer_grpc=True,
            api_key=self.api_key,
            collection_name=self.collection_name,
        )

    @classmethod
    def from_config(cls, config: Config, api_key: str) -> QdrantVectorStore:
        url = config.get("qrant", "url")
        collection_name = config.get("qrant", "collection_name")
        return cls(url, api_key, collection_name)
