from setuptools import setup, find_packages

setup(
    name="DataFlow",
    version="0.0.1",
    packages=find_packages(),
)
