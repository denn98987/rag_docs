import argparse
import os
import json
import multiprocessing
from multiprocessing import Pool
from dataclasses import asdict, dataclass
import typing as tp
import prefect.futures
from tqdm import tqdm

import prefect
from prefect import task, flow
from dotenv import load_dotenv

from DataFlow.component.config import Config
from DataFlow.component.reader import get_reader
from DataFlow.component.embedder import HuggingFaceEmbedder
from DataFlow.component.vector_store import QdrantVectorStore
from DataFlow.component.text_processor import PDFBookmarkExtractor, DataCleaner
from DataFlow.component.splitter import ChapterSplitter, SentenceSplitter
import langchain.schema.document as lang_doc
from prefect.task_runners import ConcurrentTaskRunner, SequentialTaskRunner
from prefect_dask.task_runners import DaskTaskRunner
from DataFlow.models import RawDocument, TableContent

load_dotenv()

COUNT_RETRIES = 2
SUPPORTED_TYPES = ["docx", "doc", "pdf", "txt"]

@dataclass
class Meta:
    key: str

def list_files(folder_path):
    return [
        os.path.join(folder_path, f)
        for f in os.listdir(folder_path)
        if os.path.isfile(os.path.join(folder_path, f))
    ]

@task(retries=COUNT_RETRIES)
def read_file(file_path: str) -> tp.Tuple[str, RawDocument]:
    return file_path, get_reader(file_path).read_file(file_path)

def get_bookmarks(fp: str) -> tp.Tuple[str, TableContent]:
    return (
        fp,
        PDFBookmarkExtractor().extract_bookmarks(fp, None),
    )

def save_to_file(data, file_path):
    with open(file_path, 'w') as f:
        json.dump(data, f)

def load_from_file(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)

@task(retries=COUNT_RETRIES)
def extract_bookmarks(file_path: tp.List[str]) -> tp.Tuple[str, tp.Optional[TableContent]]:
    return get_bookmarks(file_path)

@task(retries=2)
def extract_chapters(document: RawDocument, bookmarks: tp.Optional[TableContent]) -> tp.List[RawDocument]:
    if bookmarks is not None:
        chapters = ChapterSplitter().split(bookmarks, document)

    return [document]

@task(retries=COUNT_RETRIES)
def clean_document(document: RawDocument) -> RawDocument:
    return DataCleaner().process(document)

@task(retries=COUNT_RETRIES)
def split_to_sentences(document: RawDocument) -> tp.List[RawDocument]:
    return SentenceSplitter().split(document)

# @task(retries=COUNT_RETRIES)
def convert_to_embed_docs(document: RawDocument) -> lang_doc.Document:
    return lang_doc.Document(
                page_content=" ".join([p.text for p in document.data]),
                metadata=document.metadata.__dict__,
            )

@task
def embed_qrant(documents: tp.List[lang_doc.Document], config: Config):
    qrant_api_key = os.getenv("QRANT_API_KEY")
    embedding = HuggingFaceEmbedder.from_config(config)
    qrant_store = QdrantVectorStore.from_config(config, qrant_api_key)

    qrant_store.store(documents, embedding)


def read_config(config_path: str):
    return Config(config_path)

def merge_docs_tables(documents: tp.Tuple[str, RawDocument], bookmarks: tp.Tuple[str, TableContent]) -> tp.List[tp.Tuple[RawDocument, TableContent]]:
    result = {}
    for doc in documents:
        result[doc[0]] = (doc[1], )
    
    for tc in bookmarks:
        if not tc[0] in result:
            continue
        result[tc[0]] = (result[tc[0]][0], tc[1])
    
    return result


def order_docs_tables(documents: tp.Tuple[str, RawDocument], bookmarks: tp.Tuple[str, TableContent]) -> tp.Tuple[tp.List[RawDocument], tp.List[tp.Optional[TableContent]]]:
    bookmarks_dict = {}
    for tc in bookmarks:
        bookmarks_dict[tc[0]] = tc[1]

    bookmarks_result = []
    document_result = []
    for doc in documents:
        bookmarks_result.append(bookmarks_dict[doc[0]])
        document_result.append(doc[1])

    return document_result, bookmarks_result

def flat(list_lists: tp.List[tp.List[tp.Any]]) -> tp.List[tp.Any]:
    result = []
    for l in list_lists:
        result.extend(l)

    return result

def map_result(results: tp.List[prefect.futures.PrefectFuture]) -> tp.List[tp.Any]:
    return [result.result() for result in results]

@flow
def process_documents(config_path: str):
    config = read_config(config_path)
    folder_path = config.get("app", "documents_dir")
    file_paths = list_files(folder_path)

    read_results = map_result(read_file.map(file_path=file_paths))
    bookmark_results = map_result(extract_bookmarks.map(file_path=file_paths))

    ordered_pairs = order_docs_tables(read_results, bookmark_results)
    chapter_results = flat(map_result(extract_chapters.map(document=ordered_pairs[0], bookmarks=ordered_pairs[1])))
    cleaned_results = map_result(clean_document.map(document=chapter_results))
    sentence_results = flat(map_result(split_to_sentences.map(document=cleaned_results)))
    print("here: ", len(sentence_results))
    embed_docs = [convert_to_embed_docs(s) for s in sentence_results]
    print("here")
    embed_qrant(embed_docs, config)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_path", type=str, required=True)
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    process_documents(args.config_path)