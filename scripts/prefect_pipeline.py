import argparse
import os
import json
import multiprocessing
from multiprocessing import Pool
from dataclasses import asdict, dataclass
import typing as tp
from tqdm import tqdm

import prefect
from prefect import task, flow
from dotenv import load_dotenv

from DataFlow.component.config import Config
from DataFlow.component.reader import get_reader
from DataFlow.component.embedder import HuggingFaceEmbedder
from DataFlow.component.vector_store import QdrantVectorStore
from DataFlow.component.text_processor import PDFBookmarkExtractor, DataCleaner
from DataFlow.component.splitter import ChapterSplitter, SentenceSplitter
import langchain.schema.document as lang_doc

from DataFlow.models import RawDocument, TableContent

load_dotenv()

COUNT_P = 12
SUPPORTED_TYPES = ["docx", "doc", "pdf", "txt"]

@dataclass
class Meta:
    key: str

def list_files(folder_path):
    return [
        os.path.join(folder_path, f)
        for f in os.listdir(folder_path)
        if os.path.isfile(os.path.join(folder_path, f))
    ]

def read_file(fp: str):
    return get_reader(fp).read_file(fp)

def get_bookmarks(fp: str):
    return (
        fp,
        PDFBookmarkExtractor().extract_bookmarks(fp),
        PDFBookmarkExtractor().extract_bookmarks(fp, False),
    )

def save_to_file(data, file_path):
    with open(file_path, 'w') as f:
        json.dump(data, f)

def load_from_file(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)

@task(retries=2)
def read_task(folder_path: str, output_dir: str):
    file_paths = list_files(folder_path)
    results = []

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        for document in tqdm(
            pool.imap_unordered(read_file, file_paths), total=len(file_paths)
        ):
            serialized_data = asdict(document)
            output_file_name = os.path.join(output_dir, document.metadata.file_name.replace(".pdf", ".jsonl"))
            save_to_file(serialized_data, output_file_name)
            results.append(output_file_name)

    return results

@task(retries=2)
def extract_bookmarks(folder_path: str, output_dir: str):
    file_paths = list_files(folder_path)
    results = []

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        for fp, higher_bookmarks, lower_bookmarks in tqdm(
            pool.imap_unordered(get_bookmarks, file_paths), total=len(file_paths)
        ):
            serialized_data = asdict(higher_bookmarks)
            output_file_name = os.path.join(output_dir, os.path.basename(fp) + "_bm.jsonl")
            save_to_file(serialized_data, output_file_name)
            results.append(output_file_name)

    return results

@task(retries=2)
def extract_chapters(documents: tp.List[str], bookmarks: tp.List[str], output_dir: str):
    results = []

    for doc_path, bm_path in zip(documents, bookmarks):
        document = load_from_file(doc_path)
        table_content = load_from_file(bm_path)

        document_instance = RawDocument(**document)
        table_content_instance = TableContent(**table_content)

        chapters = [document_instance]
        if table_content_instance is not None:
            chapters = ChapterSplitter().split(table_content_instance, document_instance)

        for ind, chapter in enumerate(chapters):
            serialized_data = asdict(chapter)
            output_file_name = os.path.join(output_dir, f"{os.path.basename(doc_path)}_{ind}.jsonl")
            save_to_file(serialized_data, output_file_name)
            results.append(output_file_name)

    return results

@task(retries=2)
def clean_documents(documents: tp.List[str], output_dir: str):
    results = []

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        for document_path in tqdm(documents):
            document = load_from_file(document_path)
            document_instance = RawDocument(**document)

            cleaned_doc = DataCleaner().process(document_instance)
            serialized_data = asdict(cleaned_doc)
            output_file_name = os.path.join(output_dir, f"{os.path.basename(document_path)}_cleaned.jsonl")
            save_to_file(serialized_data, output_file_name)
            results.append(output_file_name)

    return results

@task(retries=2)
def split_to_sentences(documents: tp.List[str], output_dir: str):
    results = []

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        for document_path in tqdm(documents):
            document = load_from_file(document_path)
            document_instance = RawDocument(**document)

            sentences = SentenceSplitter.split(document_instance)
            for ind, sentence in enumerate(sentences):
                serialized_data = asdict(sentence)
                output_file_name = os.path.join(output_dir, f"{os.path.basename(document_path)}_s{ind}.jsonl")
                save_to_file(serialized_data, output_file_name)
                results.append(output_file_name)

    return results

@task(retries=2)
def convert_to_embed_docs(documents: tp.List[str], output_dir: str):
    results = []

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        for document_path in tqdm(documents):
            document = load_from_file(document_path)
            document_instance = RawDocument(**document)

            embed_doc = lang_doc.Document(
                page_content=" ".join([p.text for p in document_instance.data]),
                metadata=document_instance.metadata.__dict__,
            )
            serialized_data = asdict(embed_doc)
            output_file_name = os.path.join(output_dir, f"{os.path.basename(document_path)}_emb.jsonl")
            save_to_file(serialized_data, output_file_name)
            results.append(output_file_name)

    return results

@task
def embed_qrant(documents: tp.List[str], config_path: str):
    config = Config(config_path)
    qrant_api_key = os.getenv("QRANT_CLOUD_URL")
    embedding = HuggingFaceEmbedder.from_config(config)
    qrant_store = QdrantVectorStore.from_config(config, qrant_api_key)

    document_instances = [lang_doc.Document(**load_from_file(doc_path)) for doc_path in documents]
    qrant_store.store(document_instances, embedding)


def read_config(config_path: str):
    return Config(config_path)

@flow
def process_documents(config_path: str):
    config = read_config(config_path)
    folder_path = config.get("app", "documents_dir")
    output_dir = config.get("app", "output_dir")

    read_results = read_task(folder_path, output_dir)
    bookmark_results = extract_bookmarks(folder_path, output_dir)
    chapter_results = extract_chapters(read_results, bookmark_results, output_dir)
    cleaned_results = clean_documents(chapter_results, output_dir)
    sentence_results = split_to_sentences(cleaned_results, output_dir)
    embed_results = convert_to_embed_docs(sentence_results, output_dir)
    embed_qrant(embed_results, config_path)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_path", type=str, required=True)
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    process_documents(args.config_path)