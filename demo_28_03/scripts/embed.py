from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
import os
import tika
from tika import parser
from langchain_text_splitters import RecursiveCharacterTextSplitter
import logging
import threading

format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")


def main():
    embeddings = HuggingFaceEmbeddings(
        model_name="/home/dennis/llp/models/all-MiniLM-L6-v2",
        model_kwargs={"device": "cuda"},
        encode_kwargs={"normalize_embeddings": True},
    )
    text_splitter = RecursiveCharacterTextSplitter(
        "\n\n",
        # Set a really small chunk size, just to show.
        chunk_size=1024,
        chunk_overlap=128,
        length_function=len,
        is_separator_regex=False,
    )
    settings = ClickhouseSettings(table="docs_28_03")
    clickhouse = Clickhouse(embeddings, config=settings)

    def extract_text(file_path):
        parsed = parser.from_file(file_path, "http://localhost:9998/tika")
        content = parsed["content"].strip()
        return content

    def split_content(document_content):
        return text_splitter.split_text(document_content)

    for doc_name, _, files in os.walk("/home/dennis/llp/demo_28_03/data"):
        for name in files:
            file_path = os.path.join(doc_name, name)
            logging.info("FILE: ", file_path)
            try:
                content = extract_text(file_path)
                logging.info("EXTRACTED CONTENT (at most 100 syms):", content[:100])
            except Exception as e:
                logging.error("ERR: ", e)
                continue
            splitted_docs_chunks = split_content(content)
            logging.info(
                f"CHUNKS: length - {len(splitted_docs_chunks)} 0th ",
                splitted_docs_chunks[:1],
            )
            doc_metadatas = []
            for _id in range(len(splitted_docs_chunks)):
                doc_metadatas.append(
                    {
                        "chunk_id": _id,
                        "doc_name": name,
                    }
                )
            clickhouse.add_texts(splitted_docs_chunks, doc_metadatas, batch_size=512)


if __name__ == "__main__":
    main()
