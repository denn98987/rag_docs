import signal, os, sys, argparse
from dotenv import load_dotenv
from langchain_openai import ChatOpenAI
from langchain.memory import ConversationBufferMemory
from langchain.chains.combine_documents.stuff import StuffDocumentsChain
import gradio as gr

from langchain import PromptTemplate
from langchain.chains.llm import LLMChain
from langchain.embeddings import HuggingFaceEmbeddings
from langchain.chains import RetrievalQA
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
from langchain.chains.combine_documents.stuff import StuffDocumentsChain
import warnings

warnings.filterwarnings("ignore")

load_dotenv()


def deploy(openai_key, model_name):
    embeddings = HuggingFaceEmbeddings(
        model_name="/home/dennis/llp/models/all-MiniLM-L6-v2",
        model_kwargs={"device": "cuda"},
        encode_kwargs={"normalize_embeddings": True},
    )

    llm = ChatOpenAI(
        temperature=0.7,
        model_name=model_name,
        api_key=openai_key,
        base_url="https://api.vsegpt.ru/v1/",
    )

    settings = ClickhouseSettings(table="docs_28_03")
    docsearch = Clickhouse(embeddings, config=settings)
    retriever = docsearch.as_retriever(search_kwargs={"k": 10})

    custom_template = """Ты помощник поисковичок. Отвечай на русском. Если в контексте пустой, вернуть  'Контекст не содержит запрашиваего.'.
    Контекст:
    {context}
    Следующий вход: {question}
    Standalone question:
    """

    CUSTOM_QUESTION_PROMPT = PromptTemplate.from_template(custom_template)
    memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)
    llm_chain = LLMChain(
        llm=llm,
        prompt=CUSTOM_QUESTION_PROMPT,
        callbacks=None,
        verbose=True,
        memory=memory,
    )
    document_prompt = PromptTemplate(
        input_variables=["page_content", "doc_name"],
        template="Context:\ncontent:{page_content}\nsource:{doc_name}",
    )
    combine_documents_chain = StuffDocumentsChain(
        llm_chain=llm_chain,
        document_variable_name="context",
        document_prompt=document_prompt,
        callbacks=None,
    )
    qa_chain = RetrievalQA(
        combine_documents_chain=combine_documents_chain,
        callbacks=None,
        verbose=True,
        retriever=retriever,
        return_source_documents=True,
    )

    def querying(query, history):
        result = qa_chain(query)
        return result["result"].strip()

    iface = gr.ChatInterface(
        fn=querying,
        chatbot=gr.Chatbot(height=600),
        textbox=gr.Textbox(
            placeholder="Надеешься чего-то найти?", container=False, scale=7
        ),
        title="Ты не найдешь!",
        theme="soft",
        examples=[
            "Что гарантирует человеку жизнь?",
        ],
        cache_examples=True,
        retry_btn="Попробовать снова",
        undo_btn="Отменить",
        clear_btn="Очистить",
        submit_btn="Отправить",
    )

    iface.launch(share=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Deploy llm chat")
    parser.add_argument(
        "model_name",
        metavar="M",
        type=str,
        nargs=1,
        help="model name as: openai/gpt-3.5-turbo",
    )
    VSEGPT_KEY = os.getenv("VSEGPT_KEY")
    args = parser.parse_args()
    print(*args.model_name)
    deploy(VSEGPT_KEY, *args.model_name)
