import os, argparse
from dotenv import load_dotenv
from langchain_openai import ChatOpenAI
from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationalRetrievalChain
from langchain_community.embeddings.openai import OpenAIEmbeddings
import gradio as gr

from langchain import PromptTemplate
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
from typing import List, AnyStr

import warnings

warnings.filterwarnings("ignore")

load_dotenv()


def deploy(
    openai_key,
    model_name,
    embedding_model_name="emb-openai/text-embedding-3-small",
    table_name="docs_01_04",
    openai_base="https://api.vsegpt.ru/v1/",
):

    embeddings = OpenAIEmbeddings(
        model=embedding_model_name,
        openai_api_base="https://api.vsegpt.ru/v1/",
        openai_api_key=openai_key,
    )

    llm = ChatOpenAI(
        temperature=0, model_name=model_name, api_key=openai_key, base_url=openai_base
    )
    llm_chat = ChatOpenAI(
        temperature=0, model_name=model_name, api_key=openai_key, base_url=openai_base
    )

    settings = ClickhouseSettings(table=table_name)
    docsearch = Clickhouse(embeddings, config=settings)
    retriever = docsearch.as_retriever(search_kwargs={"k": 10})

    custom_template = """Ты помощник поисковичок. Отвечай на русском. Если в контексте пустой, вернуть  'Контекст не содержит запрашиваего.'.
    Контекст:
    {chat_history}
    Следующий вход: {question}
    Standalone question:
    """

    CUSTOM_QUESTION_PROMPT = PromptTemplate.from_template(
        custom_template, extra_variables=["context"]
    )
    memory_rag = ConversationBufferMemory(
        memory_key="chat_history", return_messages=True, output_key="answer"
    )
    qa_chain_rag = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=retriever,
        memory=memory_rag,
        return_source_documents=True,
        condense_question_prompt=CUSTOM_QUESTION_PROMPT,
    )

    def print_source_documents(documents: List) -> AnyStr:
        return "\n\n".join(
            [
                f"Взято из файла: {doc.metadata['doc_name']} \n Chunk #{doc.metadata['chunk_id']} \n Контент: {doc.page_content}"
                for doc in documents
            ]
        )

    def clear_memory_rag():
        memory_rag.clear()

    with gr.Blocks(fill_height=True) as demo:
        with gr.Row():
            with gr.Column(scale=1):
                chatbot_rag = gr.Chatbot(label="Enpowered by RAG", height=600)
            with gr.Column(scale=1):
                chatbot_llm = gr.Chatbot(label="LLM standalone", height=600)
        chat_input = gr.MultimodalTextbox(
            interactive=True,
            file_types=None,
            placeholder="Введите сообщение...",
            show_label=False,
        )
        clear = gr.Button("Clear")

        def user_rag(history, message):
            if message["text"] is not None:
                history.append((message["text"], None))
            return history, gr.MultimodalTextbox(value=None, interactive=False)

        def user_llm(history, message):
            if message["text"] is not None:
                history.append((message["text"], None))
            return history, gr.MultimodalTextbox(value=None, interactive=False)

        def bot_rag(history):
            result = qa_chain_rag({"question": history[-1][0]})
            form_answer = (
                result["answer"].strip()
                + "\n\n  "
                + print_source_documents(result["source_documents"])
            )
            history[-1][1] = form_answer
            return history

        def bot_llm(history):
            result = llm_chat.invoke(history[-1][0])
            history[-1][1] = result.content.strip()
            return history

        chat_input.submit(
            user_rag, [chatbot_rag, chat_input], [chatbot_rag, chat_input], queue=False
        ).then(bot_rag, chatbot_rag, chatbot_rag).then(
            lambda: gr.MultimodalTextbox(interactive=True), None, [chat_input]
        )

        chat_input.submit(
            user_llm, [chatbot_llm, chat_input], [chatbot_llm, chat_input], queue=False
        ).then(bot_llm, chatbot_llm, chatbot_llm).then(
            lambda: gr.MultimodalTextbox(interactive=True), None, [chat_input]
        )
        clear.click(lambda: None, None, chatbot_rag, queue=False)
        clear.click(lambda: None, None, chatbot_llm, queue=False)
        clear.click(clear_memory_rag)

    demo.launch(share=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Deploy llm chat")
    parser.add_argument(
        "--model_name",
        metavar="M",
        type=str,
        help="model name as: openai/gpt-3.5-turbo",
    )
    VSEGPT_KEY = os.getenv("VSEGPT_KEY")

    args, _ = parser.parse_known_args()
    model_name = (
        args.model_name if args.model_name is not None else os.getenv("LLM_NAME")
    )
    OPENAI_BASE = os.getenv("OPENAI_BASE")
    deploy(VSEGPT_KEY, model_name, openai_base=OPENAI_BASE)
