from abc import ABC, abstractmethod
import asyncio
from io import StringIO
import os
import sys
from bs4 import BeautifulSoup
import fitz
from tika import parser
import pytesseract
from PIL import Image


class BaseReader(ABC):
    @abstractmethod
    def read_file(self, file_path):
        pass


class PdfReader(BaseReader):
    def read_file(self, file_path):
        data = {"data": {"pages": []}, "metadata": {}, "is_paged": True}
        try:
            doc = fitz.open(file_path)
            metadata = {
                "title": doc.metadata.get("title", ""),
                "author": doc.metadata.get("author", ""),
                "subject": doc.metadata.get("subject", ""),
                "keywords": doc.metadata.get("keywords", ""),
            }
            data["metadata"] = metadata

            for page_num in range(len(doc)):
                page = doc.load_page(page_num)
                text = page.get_text()
                data["data"]["pages"].append(
                    {"page_number": page_num + 1, "text": text}
                )

            doc.close()
        except Exception as e:
            print("Error:", e)
            data = None

        return data

    async def read_file_async(self, file_path):
        data = {"data": {"pages": []}, "metadata": {}, "is_paged": True}
        try:
            doc = fitz.open(file_path)
            metadata = {
                "title": doc.metadata.get("title", ""),
                "author": doc.metadata.get("author", ""),
                "subject": doc.metadata.get("subject", ""),
                "keywords": doc.metadata.get("keywords", ""),
            }
            data["metadata"] = metadata

            async def read_page(page_num):
                page = doc.load_page(page_num)
                text = page.get_text()
                return {"page_number": page_num + 1, "text": text}

            tasks = [read_page(page_num) for page_num in range(len(doc))]
            pages = await asyncio.gather(*tasks)
            data["data"]["pages"] = pages

            doc.close()
        except Exception as e:
            print("Error:", e)
            data = None

        return data


class TikaReader(BaseReader):
    def read_file(self, file_path):
        data = {"data": {"pages": []}, "metadata": {}, "is_paged": False}
        try:
            parsed = parser.from_file(file_path, xmlContent=True)
            content = parsed["content"]

            if content:
                if (
                    "<div" in content
                ):  # Check if the content is structured as HTML (like PDF)
                    data["is_paged"] = True
                    pages = self.extract_pages_from_html(content)
                    data["data"]["pages"] = pages
                else:
                    data["data"]["pages"].append(
                        {"page_number": 0, "text": content.strip()}
                    )

                data["metadata"] = parsed["metadata"]

        except Exception as e:
            print("Error:", e)
            data = None

        return data

    def extract_pages_from_html(self, html_content):
        pages = []
        xhtml_data = BeautifulSoup(html_content, "html.parser")
        for i, content in enumerate(
            xhtml_data.find_all("div", attrs={"class": "page"})
        ):
            _buffer = StringIO()
            _buffer.write(str(content))
            parsed_content = parser.from_buffer(_buffer.getvalue())

            text = parsed_content["content"].strip()
            pages.append({"page_number": i + 1, "text": text})
        return pages


class OCRReader(BaseReader):
    def read_file(self, file_path):
        data = {"data": {"pages": []}, "metadata": {}, "is_paged": False}
        try:
            _, file_extension = os.path.splitext(file_path)
            if file_extension.lower() == ".pdf":
                pages = self.extract_text_from_file(file_path)
            elif file_extension.lower() == ".djvu":
                pages = self.extract_text_from_file(file_path)
            else:
                raise ValueError("Unsupported file format")

            if pages:
                data["is_paged"] = True
                for i, page_text in enumerate(pages):
                    data["data"]["pages"].append(
                        {"page_number": i + 1, "text": page_text.strip()}
                    )

        except Exception as e:
            print("Error:", e)
            data = None

        return data

    def extract_text_from_file(self, file_path):
        pages = []
        doc = fitz.open(file_path)
        for page_num in range(len(doc)):
            page = doc.load_page(page_num)
            image = page.get_pixmap()
            img = Image.frombytes("RGB", (image.width, image.height), image.samples)
            text = pytesseract.image_to_string(img, lang="rus")
            pages.append(text)
        doc.close()
        return pages
