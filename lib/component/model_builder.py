import os
from langchain_openai import ChatOpenAI
from dotenv import load_dotenv

load_dotenv()
VSEGPT_KEY = os.getenv("VSEGPT_KEY")
OPENAI_BASE = os.getenv("OPENAI_BASE")


class ModelBuilder:
    def createVseGptModel(model, temperature):
        return ChatOpenAI(
            temperature=temperature,
            model_name=model,
            api_key=VSEGPT_KEY,
            base_url=OPENAI_BASE,
        )


class ModelBuilderV2:
    def __init__(
        self, model_name: str, temperature=0, api_key=VSEGPT_KEY, base_url=OPENAI_BASE
    ):
        self.model_name = model_name
        self.temperature = temperature
        self.api_key = api_key
        self.base_url = base_url

    def __enter__(self):
        # Initialize resources
        return ChatOpenAI(
            temperature=self.temperature,
            model_name=self.model_name,
            api_key=VSEGPT_KEY,
            base_url=OPENAI_BASE,
        )

    def __exit__(self, exc_type, exc_value, traceback):
        # Cleanup resources if necessary
        pass
