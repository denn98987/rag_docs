from abc import ABC, abstractmethod
import re
from typing import List, Dict

TC_SPLITTER = re.compile("[._ ]{3,}")
TC_BR_SPLITTER = re.compile("[._ ]{3,}\n")


class BaseTextProcessor:
    def __init__(self, data: Dict, metadata: Dict, is_paged: bool):
        self.data = data
        self.metadata = metadata
        self.is_paged = is_paged

    def process(self) -> Dict:
        raise NotImplementedError("Subclasses must implement process method")


class DataProcessor(BaseTextProcessor):
    def process(self) -> Dict:
        processed_data = []
        for page in self.data["pages"]:
            processed_text = self.clean_text(page["text"])
            processed_data.append(
                {"page_number": page["page_number"], "text": processed_text}
            )
        return {
            "data": {"pages": processed_data},
            "metadata": self.metadata,
            "is_paged": self.is_paged,
        }

    def clean_text(self, text: str) -> str:
        # Replace \x2D, or -\n with ''
        cleaned_text = re.sub(r"(\x2D|-)\n?", "", text)
        # Delete non-word symbols except punctuation marks, meaningful numbers, table symbols, and paragraphs
        cleaned_text = re.sub(r"[^\w\s.,\t\n]", "", cleaned_text)
        return cleaned_text


class TableContentExtractor(BaseTextProcessor):
    def process(self) -> Dict:
        table_content = self.extract_table_content()
        return table_content

    def extract_table_content(self) -> Dict:
        possible_titles = ["Содержание", "Оглавление", "Обзор"]
        for page in self.data["pages"]:
            page["text"] = page["text"].replace("\n..", "..")
            if any(title in page["text"] for title in possible_titles):
                lines = page["text"].split("\n")
                table_content = self.extract_table_content_forced(lines)
                if len(table_content) > 3:
                    return table_content
                if page["page_number"] > 6:
                    break
        return {}

    def extract_table_content_forced(self, lines: List[str]) -> Dict:
        table_content = {}
        for line in lines:
            parts = re.split(TC_SPLITTER, line)
            parts = list(map(lambda part: part.strip(), parts))
            if (
                len(parts) < 2
                or (parts[0].isnumeric() and parts[1].isnumeric())
                or parts[0] == ""
                or not parts[1].isnumeric()
            ):
                continue
            table_content[parts[0]] = int(parts[1])
        return table_content
