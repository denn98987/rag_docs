from abc import ABC, abstractmethod
import re
from typing import Dict
from deprecated import deprecated
import pypdfium2 as pdfium
from . import parse_table_content


def get_paged_ind(pdf, table_content):
    num_title = {v: k for k, v in table_content.items()}
    page_inds = {}
    prev_key = None
    for page_num, title in num_title.items():
        page_text = pdf[page_num - 1].get_textpage().get_text_bounded()
        ind = 0
        try:
            ind = page_text.index(title)
        except ValueError:
            pass
        if prev_key is not None:
            page_inds[prev_key].append({"page_num": page_num, "ind": ind})
        page_inds[title] = [{"page_num": page_num, "ind": ind}]
        prev_key = title
    page_inds[prev_key].append({"page_num": len(pdf), "ind": -1})
    return page_inds


def split_chapters(pdf, page_inds):
    text_parts = {}
    for title, borders in page_inds.items():
        text = ""
        left_text_ind = borders[0]["ind"]
        for left_page_ind in range(
            borders[0]["page_num"] - 1, borders[1]["page_num"] - 1
        ):
            text += (
                " "
                + pdf[left_page_ind].get_textpage().get_text_bounded()[left_text_ind:]
            )
            left_text_ind = 0
        if borders[1]["ind"] != 0:
            text += (
                " "
                + pdf[borders[1]["page_num"] - 1]
                .get_textpage()
                .get_text_bounded()[: borders[1]["ind"]]
            )
        text_parts[title] = text
    return text_parts


def clean_text(text):
    row = re.sub(r"\r\n", " ", text)
    row = re.sub(r"\d \d{3}_\d{3}", " ", row)
    row = re.sub(r"^ –", " ", row)
    return row


def split_to_sentence(text):
    sentence_parts = re.split(r"\.|\?|\!", text)
    sentence_parts = map(lambda part: part.strip(), sentence_parts)
    sentence_parts = filter(lambda part: part != "", sentence_parts)
    new_sentence_parts = []
    for ind, part in enumerate(sentence_parts):
        if ind > 0 and len(part) < 15:
            new_sentence_parts[-1] = new_sentence_parts[-1] + " " + part
        elif len(part) >= 15:
            new_sentence_parts.append(part)
    return new_sentence_parts


def split_to_tokens(sentences, token_size):
    tokens = []
    sum_len = 0
    token_sentence = []
    for sentence in sentences:
        words_count = sentence.split(" ")
        if sum_len + words_count > token_size:
            tokens.append(" ".join(token_sentence))
            token_sentence = []
            sum_len = 0
        token_sentence.append(sentence)
        sum_len += words_count

    return tokens


@deprecated
def tokenize(file_path, token_size=1024):
    pdf = pdfium.PdfDocument(file_path)
    table_content = parse_table_content(file_path)
    titled_capters = split_chapters(pdf, get_paged_ind(pdf, table_content))
    token_data = []
    for title, text in titled_capters.items():
        tokens = split_to_tokens(split_to_sentence(clean_text(text)), token_size)
        for token in tokens:
            token_data.append({"title": title, "data": token})

    return token_data
