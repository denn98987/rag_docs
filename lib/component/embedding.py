from langchain_community.embeddings import HuggingFaceEmbeddings
from dotenv import load_dotenv

load_dotenv()


def build_embedding(model_name):
    embedding = HuggingFaceEmbeddings(
        model_name=model_name,
        model_kwargs={"device": "cuda"},
        encode_kwargs={"normalize_embeddings": True},
    )
    embedding.show_progress = True
    return embedding


class EmbeddingSingleton:
    instance = None

    def createInstance(self, *args, **kwargs):
        if self.instance is None:
            self.instance = build_embedding(*args, **kwargs)
        return self.instance

    def getInstance(self):
        return self.instance


class EmbeddingBuilder:
    def __init__(self, model_name: str, device: str = "cuda"):
        self.model_name = model_name
        self.device = device

    def __enter__(self):
        # Initialize resources
        embedding = HuggingFaceEmbeddings(
            model_name=self.model_name,
            model_kwargs={"device": self.device},
            encode_kwargs={"normalize_embeddings": True},
        )
        embedding.show_progress = True
        return embedding

    def __exit__(self, exc_type, exc_value, traceback):
        # Cleanup resources if necessary
        pass
