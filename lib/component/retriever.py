from langchain_community.vectorstores import FAISS

from lib.component.embedding import EmbeddingBuilder


class FAISSBuilder:
    def __enter__(self):
        # Initialize resources
        with EmbeddingBuilder("intfloat/multilingual-e5-large") as rag_emb:
            faiss_db = FAISS.load_local(
                "/home/dennis/llp/data/faiss_nk_17_05",
                rag_emb,
                allow_dangerous_deserialization=True,
            )
            return faiss_db.as_retriever()

    def __exit__(self, exc_type, exc_value, traceback):
        # Cleanup resources if necessary
        pass
