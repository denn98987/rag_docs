import os
from datetime import datetime, timedelta
import pendulum
from langchain.schema import Document
from multiprocessing import Pool, Process, Manager
import time
import itertools

# from airflow import DAG
# from airflow.operators.python import PythonOperator
# from airflow import Dataset
# from airflow.decorators import dag, task
import sys

module_path = os.path.abspath(os.path.join("/home/dennis/llp"))
if module_path not in sys.path:
    sys.path.append(module_path)
from lib import (
    PdfReader,
    OCRReader,
    save_dicts_to_jsonl,
    DataProcessor,
    TableContentExtractor,
    ChapterSpitter,
    ParagraphSplitter,
)
import logging

now = pendulum.now()
format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")
PDF_FOLDER = "/home/dennis/Downloads/AvtoBooks"


def process_pdf_files(file_path):
    print("read")
    processed_data = None
    file_name = file_path.split("/")[-1]
    if file_name.lower().endswith(".pdf"):
        data = PdfReader().read_file(file_path)
        if (
            data is not None
            and data["is_paged"]
            and all(page["text"].strip() == "" for page in data["data"]["pages"])
        ):
            # If PDF contains only images, use OCR reader
            data = OCRReader().read_file(file_path)
        if data:
            data["metadata"]["file_name"] = file_name
            processed_data = data
    return processed_data


# @task()
def get_content_table(processed_data):
    title = (
        processed_data["metadata"]["title"]
        if "title" in processed_data["metadata"]
        and processed_data["metadata"]["title"] != ""
        else processed_data["metadata"]["file_name"]
    )
    # title = processed_data['metadata']['title']
    logging.info(f"get table content for: {title}")
    try:
        tc = TableContentExtractor(**processed_data).process()
    except Exception as e:
        logging.warning(f"error table parse for: {title}", e)
        return {}
    logging.info(f"{title} count of table content rows {len(tc.items())}")
    return tc


# @task()
def clear_data(processed_data):
    title = (
        processed_data["metadata"]["title"]
        if "title" in processed_data["metadata"]
        and processed_data["metadata"]["title"] != ""
        else processed_data["metadata"]["file_name"]
    )
    logging.info(f"clear data for: {title}")
    try:
        cleaned_data = DataProcessor(**processed_data).process()
    except Exception as e:
        logging.warning(f"error clearing data for: {title}", e)
        return processed_data
    logging.info(
        f"{title} example of clean data {cleaned_data['data']['pages'][0]['text'][:15]}"
    )
    return cleaned_data


# @task()
def split_to_chapters(cleaned_data, table_content):
    # title = cleaned_data['metadata']['title']
    title = (
        cleaned_data["metadata"]["title"]
        if "title" in cleaned_data["metadata"]
        and cleaned_data["metadata"]["title"] != ""
        else cleaned_data["metadata"]["file_name"]
    )
    logging.info(f"split to chapters for: {title}")
    try:
        chapters = ChapterSpitter().split(table_content, cleaned_data)
    except Exception as e:
        logging.warning(f"error chapter splitting for: {title}", e)
        return cleaned_data
    logging.info(f"{title} count of chapters {len(chapters)}")
    return chapters


# @task()
def split_to_paragraphs(document):
    # title = document['metadata']['title']
    title = (
        document["metadata"]["title"]
        if "title" in document["metadata"] and document["metadata"]["title"] != ""
        else document["metadata"]["file_name"]
    )
    logging.info(f"split to paragraphs to: {title}")
    try:
        chunks = ParagraphSplitter().split(document, 2048)
    except Exception as e:
        logging.warning(f"error paragraph splitting for: {title}", e)
        return document
    logging.info(f"{title} count of paragraps {len(chunks)}")
    return chunks


# @task()
def to_docs(chunks):
    return


# @task()
def chapters_to_docs(document):
    # title = document['metadata']['title']
    title = (
        document["metadata"]["title"]
        if "title" in document["metadata"] and document["metadata"]["title"] != ""
        else document["metadata"]["file_name"]
    )
    logging.info(f"chapters to docs: {title}")
    docs = []
    for chapter in document["data"]["parts"]:
        metadata = dict(document["metadata"])
        print("Here", type(chapter))
        print("here meta", document["metadata"])
        if type(chapter) == str:
            metadata["chapter_title"] = title
            docs.append({"data": chapter, "metadata": metadata})
        else:
            metadata["chapter_title"] = (
                title if type(chapter) == str else chapter["title_part"]
            )
            docs.append({"data": chapter["text"], "metadata": metadata})
    return docs


# @task()
def save_to_vectore_store():
    return


# @task()
def save_data(data, file_path):
    save_dicts_to_jsonl(data, file_path)


# @task()
def convert_to_one_chapter(data):
    # title = data['metadata']['title']
    title = (
        data["metadata"]["title"]
        if "title" in data["metadata"] and data["metadata"]["title"] != ""
        else data["metadata"]["file_name"]
    )
    title = title if title != "" else data["metadata"]["file_name"]
    logging.info(f"convert to one chapter: {title}")
    text = "\n\n".join([page["text"] for page in data["data"]["pages"]])
    return {
        "data": {"parts": {"title_part": title, "text": text}},
        "metadata": data["metadata"],
    }


def process(file_path):
    file_name = file_path.split("/")[-1]
    file_path = os.path.join(PDF_FOLDER, file_name)

    data = process_pdf_files(file_path)
    if data is None:
        return
    clean_data = clear_data(data)
    chapters = None
    if data["is_paged"]:
        tc = get_content_table(data)
        if tc is not None and len(tc) > 3:
            chapters = split_to_chapters(clean_data, tc)
        else:
            chapters = convert_to_one_chapter(data)
    else:
        chapters = convert_to_one_chapter(data)

    docs = chapters_to_docs(chapters)
    logging.info(f"len to save {len(docs)}")
    save_data(
        docs, os.path.join("/home/dennis/llp/data/jsonl_01_05", file_name + ".jsonl")
    )


# @dag(dag_id="procces_pdf", start_date=now, schedule="@daily", catchup=False)
def etl():

    # Initialize readers

    # Function to process PDF files
    # @task()

    pool = Pool(10)
    # files = [f for f in os.listdir(PDF_FOLDER) if os.path.isfile(f)]
    # files = [os.path.join(address, name) for address, _, files in os.walk(PDF_FOLDER) for name in files]
    import pathlib

    p = pathlib.Path(PDF_FOLDER)
    files = [fp.as_posix() for fp in p.rglob("*.pdf")]
    # print(files)
    pool.map(process, files, 1)
    # save_data(data)
    # load(fahrenheit)


if __name__ == "__main__":
    print("main")
    etl()
