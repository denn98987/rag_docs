from langchain_core.prompts import ChatPromptTemplate
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
from langchain_community.vectorstores import FAISS
from langchain_core.runnables import (
    RunnableLambda,
    RunnableParallel,
    RunnablePassthrough,
)
from langchain_core.output_parsers import StrOutputParser
from .model_builder import ModelBuilder
from .embedding import build_embedding
from .vectorestore import BaseVectoreStore, ClickhouseStore, FAISSStore


def combine_vectors(vectors):
    result = []
    vec1_count = len(vectors["vector1"])
    vec2_count = len(vectors["vector2"])
    for i in range(max(vec1_count, vec2_count)):
        if i < vec1_count:
            result.append(vectors["vector1"][i])
        if i < vec2_count:
            result.append(vectors["vector2"][i])
    return result


class ChatAI:
    vectore_stores = []
    rag_chain = None

    def __init__(self, chat_model):
        self.model = ModelBuilder.createVseGptModel(chat_model, 0)

    def add_vectore_store(self, vectore_store: BaseVectoreStore):
        self.vectore_stores.append(vectore_store)

    def get_runnable_retrievers(self):
        if len(self.vectore_stores) == 2:
            retrievers = self._get_2_runnable_retrievers()
        else:
            raise Exception(
                f"Cant't find handler for {len(self.vectore_stores)} stores"
            )

        chain_multivec = RunnableParallel(
            {
                "original": RunnablePassthrough(),
                "context": retrievers | RunnableLambda(combine_vectors),
            }
        )

        return chain_multivec

    def _get_2_runnable_retrievers(self):
        return RunnableParallel(
            vector1=self.vectore_stores[0].get_retriever(),
            vector2=self.vectore_stores[1].get_retriever(),
        )

    def _get_prompt(self):
        template = """Answer the question in Russian based only on the following context:
        {context}

        Question: {original}
        """
        prompt = ChatPromptTemplate.from_template(template)

        return prompt

    def build_chain(self):
        output_parser = StrOutputParser()

        chain_answer = self._get_prompt() | self.model | output_parser
        self.rag_chain = self.get_runnable_retrievers() | RunnableParallel(
            {
                "original": RunnableLambda(lambda ctx: ctx["original"]),
                "sources": RunnableLambda(lambda ctx: ctx["context"]),
                "answer": chain_answer,
            }
        )

    def ask(self, query):
        return self.rag_chain.invoke(query)

    async def ask_batch(self, queries):
        return await self.rag_chain.abatch(queries)


def get_model_13_04(model_name: str = "cohere/command-r"):
    chat_ai = ChatAI(model_name)
    clikhouse_store = ClickhouseStore("sentence-transformers/LaBSE", "car_table_13_04")
    faiss_store = FAISSStore(
        "ai-forever/sbert_large_nlu_ru",
        "/home/dennis/llp/demo_15_04/data/faiss_cars_15_04",
    )
    chat_ai.add_vectore_store(clikhouse_store)
    chat_ai.add_vectore_store(faiss_store)
    chat_ai.build_chain()

    return chat_ai


def get_simple_chat(model_name="openchat/openchat-7b"):
    llm = ModelBuilder.createVseGptModel(model_name, 0)

    return llm
