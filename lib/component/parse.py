import os, re
import pypdfium2 as pdfium


def extract_table_content_forced(text):
    rows = text.split("\r\n")
    table_content = {}
    for row in rows:
        parts = re.split("[._ ]{3,}", row)
        parts = list(map(lambda part: part.strip(), parts))
        if (
            len(parts) < 2
            or parts[0].isnumeric()
            and parts[1].isnumeric()
            or parts[0] == ""
            or not parts[1].isnumeric()
        ):
            continue
        table_content[parts[0]] = int(parts[1])

    return table_content


def find_table_content(pdf):
    for ind, page in enumerate(pdf):
        page_content = page.get_textpage()
        possible_titles = ["Оглавление", "Содержание", "Обзор"]
        for title in possible_titles:
            if page_content.search(title).get_next():
                table_content = extract_table_content_forced(
                    page_content.get_text_bounded()
                )
                if ind > 5:
                    break
                if len(table_content) > 3:
                    return table_content

    return None


def extract_tables(path="/home/dennis/Downloads/SSP/SSP RUS"):
    doc_tables = {}
    doc_tables["trash_can"] = []
    for doc_name, _, files in os.walk(path):
        for name in files:
            file_path = os.path.join(doc_name, name)
            pdf = pdfium.PdfDocument(file_path)
            table = find_table_content(pdf)
            if table is None:
                doc_tables["trash_can"].append(name)
            else:
                doc_tables[name] = table
    return doc_tables


def extract_table(file_path):
    pdf = pdfium.PdfDocument(file_path)
    return find_table_content(pdf)
