from ..model_builder import ModelBuilder
from ..embedding import build_embedding
from abc import abstractmethod

from langchain_community.vectorstores import Clickhouse, ClickhouseSettings, FAISS


class BaseVectoreStore:
    store = None

    def __init__(self, embedding_model):
        self.embedding = build_embedding(embedding_model)

    def get_retriever(self):
        return self.store.as_retriever()


class ClickhouseStore(BaseVectoreStore):
    def __init__(self, embedding_model, table_name):
        super().__init__(embedding_model)
        settings = ClickhouseSettings(table=table_name)
        clickhouse = Clickhouse(self.embedding, config=settings)
        self.store = clickhouse


class FAISSStore(BaseVectoreStore):
    def __init__(self, embedding_model, table_path):
        super().__init__(embedding_model)
        clickhouse = FAISS.load_local(
            table_path, self.embedding, allow_dangerous_deserialization=True
        )
        self.store = clickhouse
