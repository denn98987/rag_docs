from abc import ABC, abstractmethod

from langchain_core.documents import Document
import re


class BaseSplitter(ABC):
    @abstractmethod
    def split(table_content: dict) -> dict:
        pass


class ChapterSpitter(BaseSplitter):
    def split(self, table_content, input_data):
        data = input_data.get("data", {})
        metadata = input_data.get("metadata", {})
        is_paged = input_data.get("is_paged", False)

        if not is_paged:
            return {"data": {}, "metadata": {}}

        pages = data.get("pages", [])

        titled_chapters = self.split_chapters(pages, table_content)
        return {"data": {"parts": titled_chapters}, "metadata": metadata}

    def get_paged_ind(self, pages, table_content):
        num_title = {v: k for k, v in table_content.items()}
        page_inds = {}
        prev_key = None
        for page in pages:
            page_num = page.get("page_number")
            text = page.get("text")
            for page_num, title in num_title.items():
                if title in text:
                    ind = text.index(title)
                    if prev_key is not None:
                        page_inds[prev_key].append({"page_num": page_num, "ind": ind})
                    page_inds[title] = [{"page_num": page_num, "ind": ind}]
                    prev_key = title
        last_page_num = pages[-1]["page_number"]
        page_inds[prev_key].append({"page_num": last_page_num, "ind": -1})
        return page_inds

    def split_chapters(self, pages, table_content):
        page_inds = self.get_paged_ind(pages, table_content)
        text_parts = {}
        for title, borders in page_inds.items():
            text = ""
            left_text_ind = borders[0]["ind"]
            for page in pages:
                page_num = page["page_number"]
                text_content = page["text"]
                if (
                    page_num >= borders[0]["page_num"]
                    and page_num <= borders[1]["page_num"]
                ):
                    if page_num == borders[0]["page_num"]:
                        text += " " + text_content[left_text_ind:]
                        left_text_ind = 0
                    elif page_num == borders[1]["page_num"]:
                        if borders[1]["ind"] != 0:
                            text += " " + text_content[: borders[1]["ind"]]
                    else:
                        text += " " + text_content
            text_parts[title] = text.strip()
        return [
            {"title_part": title, "text": text} for title, text in text_parts.items()
        ]

    def clean_text(self, text: str):
        row = re.sub(r"\r\n", " ", text)
        row = re.sub(r"\d \d{3}_\d{3}", " ", row)
        row = re.sub(r"^ –", " ", row)
        return row

    def split_to_sentence(self, text: str):
        sentence_parts = re.split(r"\.|\?|\!", text)
        sentence_parts = map(lambda part: part.strip(), sentence_parts)
        sentence_parts = filter(lambda part: part != "", sentence_parts)
        new_sentence_parts = []
        for ind, part in enumerate(sentence_parts):
            if ind > 0 and len(part) < 15:
                new_sentence_parts[-1] = new_sentence_parts[-1] + " " + part
            elif len(part) >= 15:
                new_sentence_parts.append(part)
        return new_sentence_parts

    def split_to_tokens(self, sentences: list[str], token_size: int) -> list[str]:
        tokens = []
        sum_len = 0
        token_sentence = []
        for sentence in sentences:
            words_count = len(sentence.split(" "))
            if sum_len + words_count > token_size:
                tokens.append(" ".join(token_sentence))
                token_sentence = []
                sum_len = 0
            token_sentence.append(sentence)
            sum_len += words_count
        if token_sentence:
            tokens.append(" ".join(token_sentence))
        return tokens

    def tokenize(self, data: dict, token_size=1024):
        table_content = data["metadata"]["table_content"]
        pages = data["pages"]
        titled_chapters = self.split_chapters(pages, table_content)
        token_data = []
        for part in titled_chapters:
            title = part["title_part"]
            text = part["text"]
            tokens = self.split_to_tokens(
                self.split_to_sentence(self.clean_text(text)), token_size
            )
            for token in tokens:
                token_data.append({"title": title, "data": token})
        return token_data


class ParagraphSplitter(BaseSplitter):
    def split(self, data, CHUNK_LENGTH):
        documents = []
        title_part = data["data"]["parts"][0]["title_part"]
        text = data["data"]["parts"][1]["text"]
        paragraphs = text.split("\n\n")  # Split by paragraphs

        current_chunk = ""
        for paragraph in paragraphs:
            if len(current_chunk) + len(paragraph) <= CHUNK_LENGTH:
                current_chunk += paragraph + "\n\n"
            else:
                chapter_metadata = {**data["metadata"]}
                chapter_metadata
                documents.append(
                    Document(
                        page_content=current_chunk,
                        file_title=title_part,
                        chapter_title=None,
                    )
                )
                current_chunk = paragraph + "\n\n"

        # Add the last chunk
        if current_chunk:
            documents.append(
                Document(
                    page_content=current_chunk,
                    file_title=title_part,
                    chapter_title=None,
                )
            )

        return documents
