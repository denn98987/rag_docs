import os
from .. import DataProcessor, TableContentExtractor, PdfReader

ASSETS_DIR = "/home/dennis/llp/lib/tests/assets"


def base_test(file_name, expected_titles):
    # Process the file
    file_path = os.path.join(ASSETS_DIR, file_name)
    data = PdfReader().read_file(file_path)
    data_processor = TableContentExtractor(**data)
    processed_data = data_processor.process()

    # Assert the expected titles in the table content
    table_content = processed_data["metadata"].get("table_content", {})
    assert set(table_content.keys()) == set(expected_titles)


def test_parse_tc_parse_oglavlenie_should_return_table():
    titles = [
        "Введение",
        "Кузов",
        "Безопасность пассажиров и водителя",
        "Двигатели",
        "Трансмиссия",
        "Ходовая система",
        "Электрооборудование",
        "Отопление, климатическая установка",
        "Техническое обслуживание",
    ]
    base_test("263_POLO 2002.pdf", titles)


def test_parse_tc_parse_soderzhanie_should_return_table():
    titles = [
        "Общие положения",
        "Основные устройства системы",
        "Циркуляция охлаждающей жидкости",
        "Электрические и электронные устройства",
        "Самодиагностика",
        "Вопросы для самопроверки",
    ]
    base_test("222_Электронная система охлаждения.pdf", titles)


def test_parse_tc_parse_obzor_should_return_table():
    titles = [
        "Основы измерения массы воздуха",
        "Температура и давление воздуха",
        "Влияние температуры и давления воздуха на массу воздуха",
        "Основы сгорания топлива",
        "Соотношение воздух/топливо",
        "Нормы токсичности ОГ",
        "Термоанемометрический плёночный расходомервоздуха HFM 6",
        "Задача",
        "Место установки",
        "Конструкция",
        "Чувствительный элемент",
        "Конструкция",
        "Байпасный канал",
        "Процесс измерения",
        "Распознавание обратного потока",
        "Передача сигнала расходомера воздуха в блок управления двигателя",
        "Датчик температуры всасываемого воздуха для чувствительного элемента",
        "Техническое обслуживание",
        "Диагностика",
        "Проверка знаний",
    ]
    base_test("358_Плёночный расходомер воздуха HFM 6.pdf", titles)
