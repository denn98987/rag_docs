import pytest
import os
from .. import TikaReader, PdfReader, OCRReader

# Define test files directory
ASSETS_DIR = "/home/dennis/llp/lib/tests/assets"

# Define paths to test files
PDF_FILE_PATH = os.path.join(ASSETS_DIR, "358_Плёночный расходомер воздуха HFM 6.pdf")
OCR_FILE_PATH = os.path.join(ASSETS_DIR, "Справочник.по.диагностике.неисправностей.pdf")

# Initialize readers
pdf_reader = PdfReader()
tika_reader = TikaReader()
ocr_reader = OCRReader()


@pytest.mark.parametrize(
    "reader, file_path, page_num, expected_text",
    [
        (
            pdf_reader,
            PDF_FILE_PATH,
            4,
            "В разных местах температура и давление воздуха",
        ),
        (
            tika_reader,
            PDF_FILE_PATH,
            4,
            "В разных местах температура и давление воздуха",
        ),
        (ocr_reader, OCR_FILE_PATH, 4, "коннденсат на свечах"),
    ],
)
def test_read_file(reader, file_path, page_num, expected_text):
    result = reader.read_file(file_path)
    assert result is not None
    pages = result["data"]["pages"]
    assert len(pages) >= page_num
    assert expected_text in pages[page_num - 1]["text"].strip()
