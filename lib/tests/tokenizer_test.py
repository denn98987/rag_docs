import os

from .. import tokenize, ChapterSpitter, PdfReader, TableContentExtractor

ASSETS_DIR = "/home/dennis/llp/lib/tests/assets"


def base_test(file_name, expected_len):
    # Process the file
    file_path = os.path.join(ASSETS_DIR, file_name)
    data = PdfReader().read_file(file_path)
    data_processor = TableContentExtractor(**data)
    processed_data = data_processor.process()
    token_data = ChapterSpitter().split(processed_data)

    assert len(token_data["data"]["parts"]) == expected_len


def test_simple():
    token_data = tokenize(
        "/home/dennis/llp/lib/tests/assets/222_Электронная система охлаждения.pdf"
    )
    assert len(token_data) == 28


def test_class_tokenizer():
    base_test("222_Электронная система охлаждения.pdf", 6)
