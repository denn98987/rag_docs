import os
import gradio as gr

from langchain_core.prompts import ChatPromptTemplate
from langchain_community.vectorstores import Clickhouse, ClickhouseSettings
from langchain_community.vectorstores import FAISS
from langchain_core.runnables import (
    RunnableLambda,
    RunnableParallel,
    RunnablePassthrough,
)
from langchain_core.output_parsers import StrOutputParser
import warnings
import sys

module_path = os.path.abspath(os.path.join("/home/dennis/llp"))
if module_path not in sys.path:
    sys.path.append(module_path)
from lib import ModelBuilder, build_embedding

warnings.filterwarnings("ignore")
from dotenv import load_dotenv

load_dotenv()


def combine_vectors(vectors):
    result = []
    vec1_count = len(vectors["vector1"])
    vec2_count = len(vectors["vector2"])
    for i in range(max(vec1_count, vec2_count)):
        if i < vec1_count:
            result.append(vectors["vector1"][i])
        if i < vec2_count:
            result.append(vectors["vector2"][i])
    return result


def deploy():
    llm = ModelBuilder.createVseGptModel("openchat/openchat-7b", 0)
    model_rag = ModelBuilder.createVseGptModel("cohere/command-r", 0)

    embedding = build_embedding(model_name="sentence-transformers/LaBSE")
    sber_emb = build_embedding("cointegrated/LaBSE-en-ru")

    settings_13_04 = ClickhouseSettings(table="car_table_13_04")
    clickhouse = Clickhouse(embedding, config=settings_13_04)
    faiss_db = FAISS.load_local(
        "/home/dennis/llp/data/faiss_cars_01_05",
        sber_emb,
        allow_dangerous_deserialization=True,
    )
    clickh_retriever = clickhouse.as_retriever()
    faiss_retriever = faiss_db.as_retriever()

    retrievers = RunnableParallel(vector1=faiss_retriever, vector2=clickh_retriever)

    chain_multivec = RunnableParallel(
        {
            "original": RunnablePassthrough(),
            "context": retrievers | RunnableLambda(combine_vectors),
        }
    )

    template = """Answer the question in Russian based only on the following context:
    {context}

    Question: {original}
    """
    prompt = ChatPromptTemplate.from_template(template)
    output_parser = StrOutputParser()

    chain_answer = prompt | model_rag | output_parser
    rag_chain = chain_multivec | RunnableParallel(
        {
            "original": RunnableLambda(lambda ctx: ctx["original"]),
            "sources": RunnableLambda(lambda ctx: ctx["context"]),
            "answer": chain_answer,
        }
    )

    def print_source_documents(documents):
        return "\n\n".join(
            [
                f"Взято из файла: {doc.metadata['file_name']} \n Metadata: {doc.metadata}"
                for doc in documents
            ]
        )

    with gr.Blocks(fill_height=True) as demo:
        with gr.Row():
            with gr.Column(scale=1):
                chatbot_rag = gr.Chatbot(
                    label="RAG: cohere/command-r + документы", height=600
                )
            with gr.Column(scale=1):
                chatbot_llm = gr.Chatbot(
                    label="LLM standalone: openchat/openchat-7b", height=600
                )
        chat_input = gr.MultimodalTextbox(
            interactive=True,
            file_types=None,
            placeholder="Введите сообщение...",
            show_label=False,
        )
        clear = gr.Button("Clear")

        def user_rag(history, message):
            if message["text"] is not None:
                history.append((message["text"], None))
            return history, gr.MultimodalTextbox(value=None, interactive=False)

        def user_llm(history, message):
            if message["text"] is not None:
                history.append((message["text"], None))
            return history, gr.MultimodalTextbox(value=None, interactive=False)

        def bot_rag(history):
            result = rag_chain.invoke(history[-1][0])
            form_answer = (
                result["answer"].strip()
                + "\n\n  "
                + print_source_documents(result["sources"])
            )
            history[-1][1] = form_answer
            return history

        def bot_llm(history):
            result = llm.invoke(history[-1][0])
            history[-1][1] = result.content.strip()
            return history

        chat_input.submit(
            user_rag, [chatbot_rag, chat_input], [chatbot_rag, chat_input], queue=False
        ).then(bot_rag, chatbot_rag, chatbot_rag).then(
            lambda: gr.MultimodalTextbox(interactive=True), None, [chat_input]
        )

        chat_input.submit(
            user_llm, [chatbot_llm, chat_input], [chatbot_llm, chat_input], queue=False
        ).then(bot_llm, chatbot_llm, chatbot_llm).then(
            lambda: gr.MultimodalTextbox(interactive=True), None, [chat_input]
        )
        clear.click(lambda: None, None, chatbot_rag, queue=False)
        clear.click(lambda: None, None, chatbot_llm, queue=False)

    demo.launch(share=True)


if __name__ == "__main__":
    # parser = argparse.ArgumentParser(description='Deploy llm chat')
    # parser.add_argument('--model_name', metavar='M', type=str,
    #                 help='model name as: openai/gpt-3.5-turbo')

    deploy()
